import Vue from 'vue'
import Vuex from 'vuex'
import { createStore } from 'vuex-extensions'
Vue.use(Vuex)
export default createStore(Vuex.Store, {
    state: {
        correctArr: [],
        accountIn: false,
        user_id: '',
    },
    mutations: {
        reset(state) {
            state.correctArr = [];
        }
    },
})
