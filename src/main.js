import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Slick from 'vue-slick';
import vWow from 'v-wow';
import VAnimateCss from 'v-animate-css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css'
import VuePaginate from 'vue-paginate'
import Vuex from 'vuex';
import 'es6-promise/auto';
import store from './store/store'
import Vuelidate from 'vuelidate'


Vue.config.productionTip = false
Vue.use(vWow);
Vue.use(BootstrapVue)
Vue.use(VuePaginate)
Vue.use(IconsPlugin)
Vue.use(Slick)
Vue.use(VAnimateCss)
Vue.use(Vuex)
Vue.use(Vuelidate)


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
