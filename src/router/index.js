import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Account from '../views/Account.vue'
import About from '../views/About.vue'
import News from '../views/News.vue'
import profileEdit from '../views/profileEdit.vue'
import Products from '../views/Products.vue'
import Registration from '../views/Registration.vue'
import myPoint from '@/components/accountComponents/accountBlockComponents/myPoint'
import passedTest from '@/components/accountComponents/accountBlockComponents/passedTest'
import testBlock from '@/components/accountComponents/accountBlockComponents/testBlock'
import testResult from '@/components/accountComponents/accountBlockComponents/testResult'
import videoLesson from '@/components/accountComponents/accountBlockComponents/videoLesson'
import videoProsmotr from '@/components/accountComponents/accountBlockComponents/videoProsmotr'
import newsInnerComponent from '@/components/newsComponent/newsInnerComponent'
import productBlock from '@/components/productComponent/productBlock'
import productInnerComponent from '@/components/productComponent/productInnerComponent'
import RegistrationTest from '@/components/registerTest/registerTest'
import aboutCompany from '@/components/aboutComponents/aboutCompany'
import aboutHistory from '@/components/aboutComponents/aboutHistory'
import aboutPhilosophy from '@/components/aboutComponents/aboutPhilosophy'
import aboutPhytoniring from '@/components/aboutComponents/aboutPhytoniring'
import aboutPhytoniringInfo from '@/components/aboutComponents/aboutPhytoniringInfo'
import aboutSince from '@/components/aboutComponents/aboutSince'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/profileEdit',
    component: profileEdit
  },
  {
    path: '/products',
    component: Products,
    children: [
      {
        path: '',
        component: productBlock
      },
      {
        path: '/productinner/:id',
        component: productInnerComponent
      }
    ]
  },
  {
    path: '/about',
    component: About,
    children: [
      {
        path: '',
        component: aboutCompany
      },
      {
        path: 'aboutHistory',
        component: aboutHistory
      },
      {
        path: 'aboutPhilosophy',
        component: aboutPhilosophy
      },
      {
        path: 'aboutPhytoniring',
        component: aboutPhytoniring
      },
      {
        path: 'aboutPhytoniringInfo',
        component: aboutPhytoniringInfo
      },
      {
        path: 'aboutSince',
        component: aboutSince
      },
    ]
  },
  {
    path: '/account',
    name: 'Account',
    component: Account,
    children: [
      {
        path: '',
        component: passedTest
      },
      {
        path: 'mypoint',
        component: myPoint
      },
      {
        path: 'videoLesson',
        component: videoLesson
      },
      {
        path: 'videoProsmotr',
        component: videoProsmotr
      },
      {
        path: 'testblock',
        component: testBlock
      },
      {
        path: 'testResult',
        component: testResult
      }
    ]
  },
  {
    path: '/news',
    component: News,
  },
  {
    path: '/newsInner/:id', props: true,
    component: newsInnerComponent
  },
  {
    path: '/Registration',
    component: Registration,
  },
  {
    path: '/RegistrationTest',
    component: RegistrationTest
  }
]

const router = new VueRouter({
  routes
})

export default router
